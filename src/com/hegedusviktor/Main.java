package com.hegedusviktor;

public class Main {

    public static void main(String[] args) {
	// write your code here
        PoliticalParty A = new PoliticalParty("PP1", 1803282);
        PoliticalParty B = new PoliticalParty("PP2", 555286);
        PoliticalParty C = new PoliticalParty("PP3", 339249);
        PoliticalParty D = new PoliticalParty("PP4", 228350);
        PoliticalParty E = new PoliticalParty("PP5", 219731);
        //PoliticalParty F = new PoliticalParty("Barnak", 3100);

        DHondt dHondt = new DHondt(21, 5);
        dHondt.addParty(A);
        dHondt.addParty(B);
        dHondt.addParty(C);
        dHondt.addParty(D);
        dHondt.addParty(E);
        //dHondt.addParty(F);

        dHondt.createDHondtTable();

        System.out.println("\nExample found on: https://hu.wikipedia.org/wiki/D%E2%80%99Hondt-m%C3%B3dszer");
        System.out.println("Create D'Hondt table\n");
        dHondt.printTable();

        System.out.println();
        dHondt.printNumberOfMandates();

    }
}
