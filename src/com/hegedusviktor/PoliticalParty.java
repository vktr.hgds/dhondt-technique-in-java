package com.hegedusviktor;

public class PoliticalParty implements Comparable<PoliticalParty>{
    private String name;
    private int votes;

    public PoliticalParty(String name, int votes) {
        this.name = name;
        this.votes = votes;
    }

    public String getName() {
        return name;
    }

    public int getVotes() {
        return votes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public int compareTo(PoliticalParty politicalParty) {
        int compareVotes = politicalParty.getVotes();
        return compareVotes - this.getVotes();
    }
}
