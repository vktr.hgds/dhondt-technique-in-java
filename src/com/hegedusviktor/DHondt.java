package com.hegedusviktor;
import java.util.*;

public class DHondt implements DHondtMethods {

    private int mandate;
    private int partiesCount;
    private PoliticalParty[][] politicalParties;
    int count = 0;


    //parametric constructor
    public DHondt(int mandate, int partiesCount) {
        this.partiesCount = partiesCount;
        this.mandate = mandate;
        this.politicalParties = new PoliticalParty[mandate][partiesCount];
    }


    //add party to array with parameters String and int
    public boolean addParty (String name, int votes){
        if (count >= partiesCount) {
            System.out.println("Cannot add more party to the list");
            return false;
        }

        if (votes < 0) {
            System.out.println("Number of votes cannot be lower than zero.");
            return false;
        }

        PoliticalParty tmp = new PoliticalParty(name, votes);
        politicalParties[0][count] = tmp;
        count++;
        return true;
    }


    //add politicalParty to array with parameter PoliticalParty
    public boolean addParty (PoliticalParty politicalParty) {
        if (count >= partiesCount) {
            System.out.println("Cannot add more politicalParty to the list");
            return false;
        }

        if (politicalParty.getVotes() < 0) {
            System.out.println("Number of votes cannot be lower than zero.");
            return false;
        }

        politicalParties[0][count] = politicalParty;
        count++;
        return true;
    }


    //create D'Hondt table
    public PoliticalParty[][] createDHondtTable() {

        for (int i = 1; i < mandate; i++) {
            for (int j = 0; j < partiesCount; j++) {
               int division = i + 1;
               PoliticalParty current = new PoliticalParty(politicalParties[0][j].getName(), politicalParties[0][j].getVotes() / division);
               politicalParties[i][j] = current;
            }
        }
        return politicalParties;
    }


    //print table to console
    public void printTable() {
        printPartyNames();
        for (int i = 0; i < mandate; i++) {
            for (int j = 0; j < partiesCount; j++) {
                System.out.print(j == partiesCount - 1 ? politicalParties[i][j].getVotes() + "" : politicalParties[i][j].getVotes() + "  -  ");
            }
            System.out.println();
        }
    }


    //print party names
    public void printPartyNames() {
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < partiesCount; j++) {
                System.out.print(j == partiesCount - 1 ? politicalParties[i][j].getName() + "" : politicalParties[i][j].getName() + "    -    ");
            }
            System.out.println();
        }
    }


    //print number of mandates per party
    public void printNumberOfMandates () {
        Map<String, Integer> mandatesMap = countMandates();

        for (Map.Entry<String, Integer> current : mandatesMap.entrySet()) {
            System.out.print("PoliticalParty: " + current.getKey() + ", mandates: " + current.getValue());
            float rate = (float) current.getValue() / mandate;
            rate = rate * 100;
            String formattedRate = String.format("%.02f", rate);
            System.out.println("   -   " + formattedRate + "%");
        }
    }


    // create a map from politicalParties: key = name, value = number of the mandates they got
    private Map<String, Integer> countMandates () {

        System.out.println("All mandates: " + mandate);
        List<String> partyNames = getSortedPartyNames();
        Map<String, Integer> occurrences = new HashMap<String, Integer>();

        for(String current : partyNames){
            if (!occurrences.containsKey(current)){
                occurrences.put(current, 1);
            } else {
                occurrences.put(current, occurrences.get(current)+1);
            }
        }
        return occurrences;
    }


    // create a list of strings containing the party names of the sorted list
    private List<String> getSortedPartyNames () {
        List<PoliticalParty> sortedVotes = createFinalArray();
        List<String> partyNames = new ArrayList<String>();

        for (int i = 0; i < sortedVotes.size(); i++) {
            partyNames.add(sortedVotes.get(i).getName());
        }
        return partyNames;
    }


    //cut the length of the list to the value of the mandate variable
    private List<PoliticalParty> createFinalArray() {
        List<PoliticalParty> sortedVotes = sortVotes();
        List<PoliticalParty> cutLengthToMandate = new ArrayList<PoliticalParty>();
        int valid = 0;

        for (PoliticalParty current : sortedVotes) {
            if (valid < mandate) {
                cutLengthToMandate.add(current);
            } else break;
            valid++;
        }

        return cutLengthToMandate;
    }


    //sort number of votes
    private List<PoliticalParty> sortVotes() {
        List<PoliticalParty> listOfParties = createListFromArray();
        Collections.sort(listOfParties);
        return listOfParties;
    }


    //create a list to use collection interface's sort method
    private List<PoliticalParty> createListFromArray() {
        PoliticalParty[] tmp = fillWithValues();
        ArrayList<PoliticalParty> newList = new ArrayList<PoliticalParty>();

        for (int i = 0; i < tmp.length; i++) {
            newList.add(tmp[i]);
        }

        return newList;
    }


    //fill the 1d array with the values of the 2d array
    //its an easier format to work with (sorting values)
    private PoliticalParty[] fillWithValues () {
        PoliticalParty[] tmp = new PoliticalParty[partiesCount * mandate];

        int m = 0;
        for (int i = 0; i < mandate; i++) {
            for (int j = 0; j < partiesCount; j++) {
                PoliticalParty current = new PoliticalParty(politicalParties[i][j].getName(), politicalParties[i][j].getVotes());
                tmp[m] = current;
                m++;
            }
        }
        return tmp;
    }

}
