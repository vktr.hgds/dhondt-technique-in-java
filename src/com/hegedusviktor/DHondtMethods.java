package com.hegedusviktor;

public interface DHondtMethods {

    boolean addParty(String name, int votes);
    boolean addParty(PoliticalParty politicalParty);
    void printPartyNames();
    void printTable();
    void printNumberOfMandates();
}
